# Build the Chromewm extension

load("@io_bazel_rules_closure//closure:defs.bzl", "closure_js_binary")
load("@io_bazel_rules_closure//closure:defs.bzl", "closure_js_library")
load("@io_bazel_rules_closure//closure:defs.bzl", "closure_css_library")
load("@io_bazel_rules_closure//closure:defs.bzl", "closure_css_binary")


############## Externs ##############

# Chrome API
genrule(
  name = "chrome_js",
  outs = ["chrome.js"],
  srcs = ["@externs_chrome//file"],
  cmd =  "cp $< $@",
)

# Chrome Extensions API
genrule(
  name = "chrome_extensions_js",
  outs = ["chrome_extensions.js"],
  srcs = ["@externs_chrome_extensions//file"],
  cmd =  "cp $< $@",
)

# Externs library
closure_js_library(
	name = "externs",
	srcs = [
	    ":chrome_extensions_js",
	    ":chrome_js",
	]
)


############## options.js ##############

closure_js_library(
    name = "options_lib",
    srcs = ["js/options.js"],
    deps = [
        "@io_bazel_rules_closure//closure/library",
        ":externs",
    ],
)

closure_js_binary(
	name = "options",
	deps = [":options_lib"],
	entry_points = ["chromewm.options"],
)


############## popup.js ##############

closure_js_library(
    name = "popup_lib",
    srcs = ["js/popup.js"],
    deps = [
        "@io_bazel_rules_closure//closure/library",
        ":externs",
    ],
)

closure_js_binary(
	name = "popup",
	deps = [":popup_lib"],
	entry_points = ["chromewm.popup"],
)


############## background.js ##############

closure_js_library(
    name = "background_lib",
    srcs = [
        "js/background.js",
        "js/indxDB.js",
    ],
    deps = [
        "@io_bazel_rules_closure//closure/library",
		":externs",
    ],
    suppress = [
        "reportUnknownTypes", # TODO(educampi): Remove and Fix warnings
	],
)

closure_js_binary(
	name = "background",
	deps = [":background_lib"],
	entry_points = ["chromewm.background"],
)


############## CSS Files ##############

closure_css_library(
	name = "style_css_lib",
	srcs = ["css/style.css"]
)

closure_css_library(
	name = "options_css_lib",
	srcs = ["css/options.css"]
)

closure_css_library(
	name = "popup_css_lib",
	srcs = ["css/popup.css"]
)

closure_css_binary(
	name = "options_compiled",
	deps = [
		":options_css_lib",
		":style_css_lib",
	],
	renaming = False,
)

closure_css_binary(
	name = "popup_compiled",
	deps = [
		":popup_css_lib",
		":style_css_lib",
	],
	renaming = False,
)

############## Static files ##############

filegroup(
	name = "static",
	srcs = glob([
	    "html/*",
	    "static/*",
		"css/material.min.css",
    ]),
)

############## Package ##############

# TODO(educampi): Uncompress zip file as well so it can be loaded by the browser
genrule(
	name = "extension",
	srcs = [
	    ":options.js",
		":popup.js",
	    ":background.js",
		":popup_compiled.css",
		":options_compiled.css",
	    ":static",
	],
	outs = ["extension.zip"],
	cmd = "zip -j $(OUTS) $(SRCS)",
	message = "Creating ZIP file",
)