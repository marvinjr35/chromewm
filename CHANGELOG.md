# CHANGE LOG

## 0.9.8
### New Features
- [Issue #12](https://gitlab.com/EduCampi/chromewm/issues/12): Being able to make a window persistent across workspaces.  
- [Issue #18](https://gitlab.com/EduCampi/chromewm/issues/18): Keyboard shortcuts to switch to a specific workspace.  
### Enhancements
- [Issue #3](https://gitlab.com/EduCampi/chromewm/issues/3): Implement Bazel as the build tool.  
- [Issue #14](https://gitlab.com/EduCampi/chromewm/issues/14): Create flags to identify the OS the extension is running on.  
- [Issue #22](https://gitlab.com/EduCampi/chromewm/issues/22): Implement options page and improve popup.  
- [Issue #24](https://gitlab.com/EduCampi/chromewm/issues/24): Remember windows in Full Screen mode.  

## 0.9.7
### Enhancements
- [Issue #15](https://gitlab.com/EduCampi/chromewm/issues/15): Identifies initialization due to New installation, Upgrade, or Browser crash. It does not reload windows in case of an upgrade.
- [Issue #16](https://gitlab.com/EduCampi/chromewm/issues/16): On New Installation, shows notification asking to configure keyboard shortcuts.

### Fixed Bugs
- [Issue #13](https://gitlab.com/EduCampi/chromewm/issues/13): Window Tiling not working properly in some display layouts
