/**
 * @fileoverview Options file
 * @author EduCampi
*/
goog.provide('chromewm.options');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.string');


goog.events.listenOnce(
    goog.dom.getDocument(),
    goog.events.EventType.DOMCONTENTLOADED,
    () => {
      /** @type {!chromewm.options} optionsPage */
      const optionsPage = new chromewm.options();
      optionsPage.init();
    }
);


/**
 * Contructor for the Options Object
 * @constructor @export
 */
chromewm.options = function() {
  /** @const @private {?Element} */
  this.showNotificationsDom_ = goog.dom.getElement('showNotifications');
  /** @const @private {?Element} */
  this.loopWorkspacesDom_ =  goog.dom.getElement('loopWorkspaces');
  /** @const @private {?Element} */
  this.workspaceQtyDom_ = goog.dom.getElement('workspaceQty');
};


/**
 * Sets listeners and options value
 * @export
 */
chromewm.options.prototype.init = function() {

  if ((this.showNotificationsDom_ == null) ||
      (this.loopWorkspacesDom_ == null) ||
      (this.workspaceQtyDom_ == null)) {
    return;
  }
  
  this.getFromStorage_();

  goog.events.listen(
      goog.dom.getDocument(),
      goog.events.EventType.CHANGE,
      () => {
        this.saveToStorage_();
      }
  );

  goog.events.listen(
      goog.dom.getElement('confCommands'),
      goog.events.EventType.CLICK,
      () => {
        chrome.tabs.create({'url': "chrome://extensions/shortcuts"});
      }
  );
};


/**
 * Gets saved options values from the storage
 * @private
 */
chromewm.options.prototype.getFromStorage_ = function() {
  
  chrome.storage.sync.get({
      'showNotifications': true,
      'loopWorkspaces': false,
      'workspaceQty': ''
      },
      (savedOptions) => {
        setMdlSwitchStatus_(this.showNotificationsDom_,
            savedOptions['showNotifications']);

        setMdlSwitchStatus_(this.loopWorkspacesDom_,
            savedOptions['loopWorkspaces']);

        if (!goog.string.isEmptyOrWhitespace(savedOptions['workspaceQty'])) {
          this.workspaceQtyDom_['value'] = savedOptions['workspaceQty'];
          this.workspaceQtyDom_['labels'][0]['hidden'] = true;
        }
      }
  );
};


/**
 * Saves option values to storage
 * @private
 */
chromewm.options.prototype.saveToStorage_ = function() {
  
  if (/[1-9]/.test(this.workspaceQtyDom_['value'])) {
    chrome.storage.sync.set({
        'workspaceQty': goog.string.makeSafe(this.workspaceQtyDom_['value'])
    });
  }
  
  chrome.storage.sync.set({
      'showNotifications': this.showNotificationsDom_['checked'],
      'loopWorkspaces': this.loopWorkspacesDom_['checked']
  });
};
  

/**
 * Helper function to set the state of a Material Design Lite switch
 * @param {?Element} switchDom - switch Element
 * @param {boolean} checked
 * @private
 */
const setMdlSwitchStatus_ = (switchDom, checked) => {

  if (switchDom == null) return;

  if (checked) {
    switchDom.parentElement['MaterialSwitch']['on']();
  } else {
    switchDom.parentElement['MaterialSwitch']['off']();
  }
};